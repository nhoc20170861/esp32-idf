#include "gpio_input.h"

input_callback_t input_callback = nullptr;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    uint32_t gpio_input = (uint32_t) arg;
    input_callback(gpio_input);
}

void input_io_create(gpio_num_t gpio_input, gpio_int_type_t intr_type ){

    gpio_pad_select_gpio(gpio_input);
    gpio_set_direction(gpio_input, GPIO_MODE_INPUT);
    gpio_set_pull_mode(gpio_input, GPIO_PULLUP_ONLY);
    gpio_set_intr_type(gpio_input, intr_type);
    gpio_intr_enable(gpio_input);
    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(gpio_input, gpio_isr_handler, (void*) gpio_input);
}

int input_io_getLevel(gpio_num_t gpio_input)
{
    return gpio_get_level(gpio_input);
}

void input_set_callback(input_callback_t cb) {
    input_callback = cb;
}