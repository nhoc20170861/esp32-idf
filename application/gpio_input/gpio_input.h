#pragma once
#include <driver/gpio.h>

#define ESP_INTR_FLAG_DEFAULT 0
typedef void (*input_callback_t)(int);

void input_io_create(gpio_num_t gpio_input, gpio_int_type_t intr_type );

int input_io_getLevel(gpio_num_t gpio_input);

void input_set_callback(input_callback_t cb);