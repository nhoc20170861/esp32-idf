#pragma once

// include library
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "driver/uart.h"
#include "driver/gpio.h"
#include "esp_log.h"

#define UART_TXD2 (GPIO_NUM_17)
#define UART_RXD2 (GPIO_NUM_16)
#define UART_PORT UART_NUM_2
#define UART_RTS  (UART_PIN_NO_CHANGE)
#define UART_CTS  (UART_PIN_NO_CHANGE)
static const int BUF_SIZE = 1024;

void initUart(void);
void sendData(const char *data);
