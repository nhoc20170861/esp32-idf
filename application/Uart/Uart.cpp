#include "Uart.h"

static const char *UART_TAG = "UART TEST";


void initUart(void)
{
    const uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .rx_flow_ctrl_thresh = 122,
        .source_clk = UART_SCLK_APB,
    };

    // Set UART log level
    esp_log_level_set(UART_TAG, ESP_LOG_INFO);

    uart_driver_install(UART_PORT, BUF_SIZE * 2, 0, 0, NULL, 0);

    // Configure UART parameters
    uart_param_config(UART_PORT, &uart_config);

    uart_set_pin(UART_PORT, UART_TXD2, UART_RXD2, UART_RTS, UART_CTS);

    ESP_LOGI(UART_TAG, "UART set pins, mode and install driver.");
}

void sendData(const char *data)
{
    const int len = strlen(data);
    uart_write_bytes(UART_PORT, data, len);
    ESP_LOGI(UART_TAG, "Data Send: %s\n", data);
}
