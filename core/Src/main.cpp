
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "main.h"
#include "esp_log.h"
#define LOG_TAG "Main"

void chuongtrinhcambien(void)
{
    // DHT11 DHT2  , pt100 pt1000
    nhietdo++;
    doam = doam + 2;
}

void functionTransmitterUart(void *arg)
{

    while (1)
    {
        chuongtrinhcambien();
        DataJson(nhietdo, doam, TB1, TB2, C1, C2, 10, 10);
        // sendData(TX_TASK_TAG, "Hello world!!!\r\n");
        delay(1000);
    }
    vTaskDelete(NULL);
}
static const int RX_BUF_SIZE = 1024;
void functionReceiveUart(void *arg)
{
    static const char *RX_TASK_TAG = "RX_TASK";
    esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);
    
    uint8_t *data = (uint8_t *)malloc(RX_BUF_SIZE + 1);

    while (1)
    {

        const int rxBytes = uart_read_bytes(UART_NUM_2, data, RX_BUF_SIZE, 500 / portTICK_RATE_MS);
        if (rxBytes > 0)
        {
            data[rxBytes] = 0;

            ESP_LOGI(RX_TASK_TAG, "Read %d bytes: '%s'", rxBytes, data);

            // ESP_LOG_BUFFER_HEXDUMP(RX_TASK_TAG, data, rxBytes, ESP_LOG_INFO);

            // printf("Data nhan duoc: %s\n", data);

            /*
            đưa dữ liệu đọc được vào thư viện JSON
            kiểm tra chuỗi đọc được có đúng với cấu trúc JSON
            Xử lý mục đích theo khung truyền dữ liệu JSON
            */

            // char *bufData = (char*)calloc(RX_BUF_SIZE + 1, sizeof(data));
            // snprintf(bufData, RX_BUF_SIZE + 1, "%s", (char *)data);

            // printf("bufData: %s\n", bufData);

            // str_json = cJSON_Parse(bufData);
            // /*
            // Nếu str_json trả về False lỗi dữ liệu JSON
            // */
            // if (!str_json)
            // {
            //     printf("Data JSON ERROR!!!\n");
            // }
            // else
            // {
            //     printf("Data JSON OK!!!\n");

            //     // xử lý theo mục đích của mình

            //     // 	{"TB1":"1"}  {"TB1":"0"}

            //     //	{"TB2":"1"}  {"TB2":"0"}

            //     cJSON_Delete(str_json);

            //     free(bufData);
            // }
        }
        delay(100);
    }
    vTaskDelete(NULL);
}
esp_err_t Main::setup(void)
{

    ESP_LOGI(LOG_TAG, "Setup!");

    esp_err_t status{ESP_OK};
    status |= led.init();

    initUart();

    // task send data through uatt
    //xTaskCreate(functionTransmitterUart, "uart_tx", 2048 * 2, NULL, 3, NULL);

    // task receive data through uatt
    xTaskCreate(functionReceiveUart, "uart_rx", 2048 * 2, NULL, 3, NULL);

    return status;
}

void Main::loop(void)
{
    led.set(HIGH);
    ESP_LOGI(LOG_TAG, "Led on");
    delay(1000);
    led.set(LOW);
    ESP_LOGI(LOG_TAG, "Led off");
    sendData("Hello world!!!\r\n");
    delay(1000);
}

static Main my_main;

extern "C" void app_main(void)
{
    ESP_ERROR_CHECK(my_main.setup());
    while (true)
    {

        my_main.loop();
    }
}
