#pragma once
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "Gpio.h"
#include "Uart.h"
#include "cJSON.h"
#include "Wifi.h"
cJSON *str_json, *str_TB1, *str_TB2, *str_C1, *str_C2, *usr;

int nhietdo = 0;
int doam = 0;
int TB1 = 0;
int TB2 = 0;
int C1 = 100;
int C2 = 200;
char JSON[100];
char Str_ND[100];
char Str_DA[100];
char Str_TB1[100];
char Str_TB2[100];
char Str_C1[100];
char Str_C2[100];
char Length[100];


class Main final
{
private:
    /* data */
public:
    esp_err_t setup(void);
    void loop(void);
    Gpio::GpioOutput led{GPIO_NUM_2, true};
};
void delay(uint32_t time)
{
    vTaskDelay(time / portTICK_PERIOD_MS);
}

// Convert data to JSON
void DataJson(unsigned int ND , unsigned int DA,  unsigned int TB1,  unsigned int TB2, unsigned int C1, unsigned int C2 , unsigned int H1 ,unsigned int H2)
{
	
	char *dataJ;
	usr=cJSON_CreateObject();  
	cJSON_AddItemToObject(usr, "ND", cJSON_CreateNumber(ND)); 
	cJSON_AddItemToObject(usr, "DA", cJSON_CreateNumber(DA));  
	cJSON_AddItemToObject(usr, "TB1", cJSON_CreateNumber(TB1));  
	cJSON_AddItemToObject(usr, "TB2", cJSON_CreateNumber(TB2));  	
	cJSON_AddItemToObject(usr, "C1", cJSON_CreateNumber(C1));  
    cJSON_AddItemToObject(usr, "C2", cJSON_CreateNumber(C2));
	cJSON_AddItemToObject(usr, "H1", cJSON_CreateNumber(H1));	
	cJSON_AddItemToObject(usr, "H2", cJSON_CreateNumber(H2));		
	dataJ = cJSON_Print(usr);  
	printf("DataJ: %s\n",dataJ);

	sendData(dataJ);	

	cJSON_Delete(usr);
	free(dataJ);
	
	//cJSON_AddItemToObject(usr, "C", cJSON_CreateString("Handsome"));
	
	//https://www.programmersought.com/article/98966009006/
	
	
}

